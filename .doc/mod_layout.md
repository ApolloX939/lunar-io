# Lunar IO >> Documentation >> Mod Layout

**[Back]**

The Mod is layed out into multiple mods, mostly to keep things more organized.

## lunario (core)

This is the root mod required by all other sub-mods, it's responsible for game detection, base API, and storage via Mod Storage.

## item_io

This mod is responsible for grabbing and putting API, it also introduces a node (the "[Item Link](list_nodes.md#item-link-item_io)").

**[Back]**

[Back]: ../DOCS.md