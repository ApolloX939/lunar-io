# Lunar IO >> Documentation >> Nodes

**[Back]**

## Item Link (item_io)

This node can perform both insert and output operations on **ANY** node which has at least 1 inventory.

With the "Player Interface" upgrade, it can also insert and output to a player.

> The "Range Extension" upgrade increases the "Player Interface" by 1 node distance.

> The player must have access, even if it's minimal access, in order to use the "Player Interface" feature.

**[Back]**

[Back]: ../DOCS.md