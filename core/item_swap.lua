
if _lunario.cache.gamemode == "MTG" then
    _lunario.cache.items = {
        air = "",
        group_tree = "group:tree",
        group_wood = "group:wood",
        stick = "default:stick",
        iron_ingot = "default:steel_ingot",
        iron_block = "default:steelblock",
        gold_ingot = "default:gold_ingot",
        gold_block = "default:goldblock",
        -- Unrelyable, please verify
        copper_ingot = "default:copper_ingot",
        copper_block = "default:copperblock",
        diamond = "default:diamond",
        crystal = "default:mese_crystal",
    }
elseif _lunario.cache.gamemode == "VL" then
    local root = "mcl_core" -- For when VoxeLibre changes the internal name
    _lunario.cache.items = {
        air = "",
        group_tree = "group:tree",
        group_wood = "group:wood",
        stick = root..":stick",
        iron_ingot = root..":iron_ingot",
        iron_block = root..":ironblock",
        gold_ingot = root..":gold_ingot",
        gold_block = root..":goldblock",
        -- Unrelyable, please verify
        copper_ingot = root..":copper_ingot",
        copper_block = root..":copperblock",
        diamond = root..":diamond",
        crystal = "mesecons:redstone",
    }
end
