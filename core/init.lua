
lunario = {
    VERSION = "v1.0-dev",
    AUTHOR = "ApolloX939",
    REPO = "https://gitlab.com/ApolloX939/lunar-io",
}

_lunario = {
    log = function (msg)
        if type(msg) == "table" then
            msg = minetest.serialize(msg)
        end
        minetest.log("action", "Lunar IO >> " .. tostring(msg))
    end,
    error = function (msg)
        if type(msg) == "table" then
            msg = minetest.serialize(msg)
        end
        minetest.log("error", "Please make a new issue @ " .. lunario.REPO)
        minetest.log("error", "Lunar IO " .. lunario.VERSION)
        minetest.log("error", _VERSION)
        minetest.log("error", "Lunar IO >> " .. tostring(msg))
        error("Lunar IO >> " .. tostring(msg))
    end,
    ok = function (value)
        return {success = true, errmsg = nil, value = value}
    end,
    err = function (msg)
        if type(msg) == "table" then
            msg = minetest.serialize(msg)
        end
        return {success = false, errmsg = tostring(msg), value = nil}
    end,
    is_ok = function (value)
        if value.success == nil or value.errmsg == nil or value.value == nil then
            return false
        end
        return value.success
    end,
    val_or_nil = function (value)
        if not _lunario.is_ok(value) then
            return nil
        end
        return value.value
    end,
    exists = function (itemname)
        if minetest.registered_nodes[itemname] ~= nil then
            return _lunario.ok("node")
        elseif minetest.registered_tools[itemname] ~= nil then
            return _lunario.ok("tool")
        elseif minetest.registered_items[itemname] ~= nil or minetest.registered_craftitems[itemname] ~= nil then
            return _lunario.ok("item")
        else
            return _lunario.err("No such thing")
        end
    end,
    pos2str = function (p)
        if (type(p) ~= "table" or type(p) ~= "userdata") or type(p.x) ~= "number" or type(p.y) ~= "number" or type(p.z) ~= "number" then
            return _lunario.err("Not a vector")
        end
        p = vector.round(p)
        return _lunario.ok(minetest.pos_to_string(p))
    end,
    cache = {},
    store = minetest.get_mod_storage(),
}

local mp = minetest.get_modpath("lunario")
minetest.log("warning", "Lunar IO " .. lunario.VERSION)
minetest.log("warning", _VERSION)

_lunario.log("Prelude ...")
dofile(mp .. DIR_DELIM .. "prelude.lua")

_lunario.log("API ...")
dofile(mp .. DIR_DELIM .. "api.lua")

_lunario.log("Item Swap ...")
dofile(mp .. DIR_DELIM .. "item_swap.lua")
