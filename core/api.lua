
lunario.pack = function (stack)
    local data = stack:to_string()
    if data == nil then
        return _lunario.err("Pack failed, to_string() didn't work")
    end

    return _lunario.ok(data)
end

lunario.unpack = function (stackstring)
    local stack = ItemStack(stackstring)
    if stack == nil then
        return _lunario.err("Unpack failed, ItemStack didn't work")
    end

    return _lunario.ok(stack)
end

lunario.gen = function ()
    local choices = {"A", "B", "C", "D", "E", "F", "X", "+", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
    local hash = ""
    for i = 0, 3, 1 do
        for o = 0, 4, 1 do
            hash = hash .. choices[math.random(#choices)]
        end
        hash = hash .. "-"
    end
    return string.sub(hash, 1, string.len(hash)-1)
end

_lunario.getpack = function ()
    return minetest.deserialize(_lunario.store:get_string("lunario.datapack")) or {}
end
_lunario.setpack = function (datapack)
    _lunario.store:set_string("lunario.datapack", minetest.deserialize(datapack))
end

lunario.new_hash = function ()
    local stored = _lunario.getpack()
    local hash = lunario.gen()
    local tries = 0
    while stored[hash] ~= nil do
        hash = lunario.gen()
        tries = tries + 1
        if tries >= 25 then
            return _lunario.err("Hash >> Max tries exceeded")
        end
    end
    return _lunario.ok(hash)
end

lunario.new_playerdata = function (playername)
    local stored = _lunario.getpack()
    for h, dat in pairs(stored) do
        if dat ~= nil and dat.name = playername then
            return _lunario.err("PlayerData >> Already exists")
        end
    end
    local hash = lunario.new_hash()
    if not _lunario.is_ok(hash) then
        return hash
    end
    hash = hash.value
    stored[hash] = {
        name = playername,
        networks = {},
        waypoints = {},
    }
    _lunario.setpack(stored)
    return _lunario.ok(stored[hash])
end

lunario.get_playerdata = function (playername)
    local stored = _lunario.getpack()
    for h, dat in pairs(stored) do
        if dat ~= nil and dat.name = playername then
            return _lunario.ok(dat)
        end
    end
    return _lunario.err("PlayerData >> Not found")
end

lunario.set_playerdata = function (playerdata)
    local stored = _lunario.getpack()
    for h, dat in pairs(stored) do
        if dat ~= nil and dat.name = playerdata.name then
            stored[h] = playerdata
            _lunario.setpack(stored)
            return _lunario.ok(false)
        end
    end
    return _lunario.err("PlayerData >> Not found")
end

lunario.get_playerhash = function (playername)
    local stored = _lunario.getpack()
    for h, dat in pairs(stored) do
        if dat ~= nil and dat.name = playername then
            return _lunario.ok(h)
        end
    end
    return _lunario.err("PlayerData >> Not found")
end
