
if minetest.registered_nodes["default:stone"] ~= nil then
    _lunario.cache.gamemode = "MTG"
elseif minetest.registered_nodes["mcl_core:stone"] ~= nil then
    _lunario.cache.gamemode = "VL"
else
    _lunario.error("Unrecognized Gamemode (expected 'Minetest Game' or 'VoxeLibre')")
end
_lunario.log("Gamemode: " .. _lunario.cache.gamemode)
